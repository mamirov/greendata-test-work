package ru.mamirov.greendata.filter;

/**
 * Created by Marat on 02.02.2018.
 */
public enum Operator {
    EQ, LIKE
}
