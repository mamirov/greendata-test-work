package ru.mamirov.greendata.filter.impl;

import ru.mamirov.greendata.filter.Filter;
import ru.mamirov.greendata.filter.Operator;
import ru.mamirov.greendata.filter.PredicateItem;
import ru.mamirov.greendata.model.Client;

import java.util.ArrayList;
import java.util.List;

public class ClientFilter extends Filter<Client> {

    public ClientFilter(Client model) {
        super(model);
    }

    @Override
    public List<PredicateItem> getPredicates() {
        List<PredicateItem> predicateItems = new ArrayList<>();

        predicateItems.add(new PredicateItem("name", getModel().getName(), Operator.LIKE));
        predicateItems.add(new PredicateItem("shortName", getModel().getShortName(), Operator.LIKE));
        predicateItems.add(new PredicateItem("address", getModel().getAddress(), Operator.LIKE));
        predicateItems.add(new PredicateItem("orgLawType", getModel().getOrgLawType(), Operator.EQ));

        return predicateItems;
    }
}
