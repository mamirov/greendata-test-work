package ru.mamirov.greendata.filter.impl;

import ru.mamirov.greendata.filter.Filter;
import ru.mamirov.greendata.filter.Operator;
import ru.mamirov.greendata.filter.PredicateItem;
import ru.mamirov.greendata.model.Deposit;

import java.util.ArrayList;
import java.util.List;

public class DepositFilter extends Filter<Deposit> {
    public DepositFilter(Deposit model) {
        super(model);
    }

    @Override
    public List<PredicateItem> getPredicates() {
        List<PredicateItem> predicateItems = new ArrayList<>();

        predicateItems.add(new PredicateItem("bank", getModel().getBank(), Operator.EQ));
        predicateItems.add(new PredicateItem("client", getModel().getClient(), Operator.EQ));
        predicateItems.add(new PredicateItem("expiration", getModel().getExpiration(), Operator.EQ));
        predicateItems.add(new PredicateItem("percent", getModel().getPercent(), Operator.EQ));
        predicateItems.add(new PredicateItem("openDate", getModel().getOpenDate(), Operator.EQ));

        return predicateItems;
    }
}
