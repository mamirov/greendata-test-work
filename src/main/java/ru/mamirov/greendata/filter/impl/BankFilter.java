package ru.mamirov.greendata.filter.impl;

import ru.mamirov.greendata.filter.Filter;
import ru.mamirov.greendata.filter.Operator;
import ru.mamirov.greendata.filter.PredicateItem;
import ru.mamirov.greendata.model.Bank;
import java.util.ArrayList;
import java.util.List;

public class BankFilter extends Filter<Bank> {

    public BankFilter(Bank model) {
        super(model);
    }

    @Override
    public List<PredicateItem> getPredicates() {
        List<PredicateItem> predicateItems = new ArrayList<>();

        predicateItems.add(new PredicateItem("name", getModel().getName(), Operator.LIKE));
        predicateItems.add(new PredicateItem("bik", getModel().getBik(), Operator.EQ));

        return predicateItems;
    }
}
