package ru.mamirov.greendata.filter;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public abstract class Filter<T> implements Specification<T> {

    private T model;

    public Filter(T model) {
        this.model = model;
    }

    @Override
    public Predicate toPredicate(Root<T> tRoot, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicateList = new ArrayList<>();
        for(PredicateItem item : getPredicates()) {
            if(item.getValue() != null) {
                switch (item.getOperator()) {
                    case LIKE:
                        if(item.getValue() instanceof String) {
                            Predicate predicate = criteriaBuilder.like(tRoot.get(item.getFieldName()), item.getValue().toString());
                            predicateList.add(predicate);
                        }
                        break;
                    case EQ:
                        Predicate predicate = criteriaBuilder.equal(tRoot.get(item.getFieldName()), item.getValue());
                        predicateList.add(predicate);
                        break;
                }
            }
        }
        return criteriaBuilder.and(predicateList.toArray(new Predicate[predicateList.size()]));
    }

    public abstract List<PredicateItem> getPredicates();

    public T getModel() {
        return model;
    }

    public void setModel(T model) {
        this.model = model;
    }
}
