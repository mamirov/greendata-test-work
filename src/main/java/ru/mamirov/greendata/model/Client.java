package ru.mamirov.greendata.model;

import javax.persistence.*;

@Entity
@Table(schema = "greendata", name = "clients")
public class Client {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;
    
    @Column(name = "short_name", nullable = false)
    private String shortName;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "org_law_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private OrgLawType orgLawType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public OrgLawType getOrgLawType() {
        return orgLawType;
    }

    public void setOrgLawType(OrgLawType orgLawType) {
        this.orgLawType = orgLawType;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", shortName='" + shortName + '\'' +
                ", address='" + address + '\'' +
                ", orgLawType=" + orgLawType +
                '}';
    }
}
