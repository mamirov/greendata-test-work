package ru.mamirov.greendata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ru.mamirov.greendata.dao.ClientRepository;
import ru.mamirov.greendata.model.Client;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public Client findById(Long id) {
        return clientRepository.findOne(id);
    }

    @Override
    public Iterable<Client> findAll(Specification<Client> specification, Sort sort) {
        return clientRepository.findAll(specification, sort);
    }

    @Override
    public void delete(Long id) {
        clientRepository.delete(id);
    }

    @Override
    public Client save(Client client) {
        return clientRepository.save(client);
    }
}
