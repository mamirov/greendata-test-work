package ru.mamirov.greendata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ru.mamirov.greendata.dao.DepositRepository;
import ru.mamirov.greendata.model.Deposit;

@Service
public class DepositServiceImpl implements DepositService {

    @Autowired
    private DepositRepository depositRepository;

    @Override
    public Deposit findById(Long id) {
        return depositRepository.findOne(id);
    }

    @Override
    public Iterable<Deposit> findAll(Specification<Deposit> specification, Sort sort) {
        return depositRepository.findAll(specification, sort);
    }

    @Override
    public void delete(Long id) {
        depositRepository.delete(id);
    }

    @Override
    public Deposit save(Deposit deposit) {
        return depositRepository.save(deposit);
    }
}
