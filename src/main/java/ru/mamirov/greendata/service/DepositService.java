package ru.mamirov.greendata.service;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import ru.mamirov.greendata.model.Deposit;

public interface DepositService {

    Deposit findById(Long id);
    Iterable<Deposit> findAll(Specification<Deposit> specification, Sort sort);
    void delete(Long id);
    Deposit save(Deposit deposit);
}
