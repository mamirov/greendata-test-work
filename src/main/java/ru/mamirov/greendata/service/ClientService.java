package ru.mamirov.greendata.service;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import ru.mamirov.greendata.model.Client;
import ru.mamirov.greendata.model.OrgLawType;

import java.util.List;

/**
 * Created by Marat on 02.02.2018.
 */
public interface ClientService {

    Client findById(Long id);
    Iterable<Client> findAll(Specification<Client> specification, Sort sort);
    void delete(Long id);
    Client save(Client client);
}
