package ru.mamirov.greendata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ru.mamirov.greendata.dao.BankRepository;
import ru.mamirov.greendata.model.Bank;

@Service
public class BankServiceImpl implements BankService {

    @Autowired
    private BankRepository bankRepository;

    @Override
    public Bank findById(Long id) {
        return bankRepository.findOne(id);
    }

    @Override
    public Iterable<Bank> findAll(Specification<Bank> specification, Sort sort) {
        return bankRepository.findAll(specification, sort);
    }

    @Override
    public void delete(Long id) {
        bankRepository.delete(id);
    }

    @Override
    public Bank save(Bank bank) {
        return bankRepository.save(bank);
    }
}
