package ru.mamirov.greendata.service;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import ru.mamirov.greendata.model.Bank;

public interface BankService {

    Bank findById(Long id);
    Iterable<Bank> findAll(Specification<Bank> specification, Sort sort);
    void delete(Long id);
    Bank save(Bank bank);
}
