package ru.mamirov.greendata.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import ru.mamirov.greendata.filter.impl.DepositFilter;
import ru.mamirov.greendata.model.Deposit;
import ru.mamirov.greendata.service.DepositService;

@RestController
@RequestMapping(value = "/api/v1/deposit")
public class DepositController {

    @Autowired
    private DepositService depositService;

    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Deposit> getDeposits(Deposit deposit, Sort sort) {
        return depositService.findAll(new DepositFilter(deposit), sort);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Deposit getDepositById(@PathVariable Long id) {
        return depositService.findById(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Deposit createDeposit(@RequestBody Deposit deposit) {
        return depositService.save(deposit);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteDeposit(@PathVariable Long id) {
        depositService.delete(id);
    }
}
