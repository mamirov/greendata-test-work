package ru.mamirov.greendata.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import ru.mamirov.greendata.filter.impl.BankFilter;
import ru.mamirov.greendata.model.Bank;
import ru.mamirov.greendata.service.BankService;

@RestController
@RequestMapping(value = "/api/v1/bank")
public class BankController {

    @Autowired
    private BankService bankService;

    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Bank> getBanks(Bank bank, Sort sort) {
        return bankService.findAll(new BankFilter(bank), sort);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Bank getBankById(@PathVariable Long id) {
        return bankService.findById(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Bank createBank(@RequestBody Bank bank) {
        return bankService.save(bank);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteBank(@PathVariable Long id) {
        bankService.delete(id);
    }
}
