package ru.mamirov.greendata.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import ru.mamirov.greendata.filter.impl.ClientFilter;
import ru.mamirov.greendata.model.Client;
import ru.mamirov.greendata.service.ClientService;

@RestController
@RequestMapping(value = "/api/v1/client")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Client> getClients(Client client, Sort sort) {
        System.out.println(client);
        return clientService.findAll(new ClientFilter(client), sort);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Client getClientById(@PathVariable Long id) {
        return clientService.findById(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Client createClient(@RequestBody Client client) {
        return clientService.save(client);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteClient(@PathVariable Long id) {
        clientService.delete(id);
    }
}
