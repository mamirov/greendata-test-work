package ru.mamirov.greendata.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.mamirov.greendata.model.Bank;

public interface BankRepository extends PagingAndSortingRepository<Bank, Long>, JpaSpecificationExecutor<Bank> {
}
