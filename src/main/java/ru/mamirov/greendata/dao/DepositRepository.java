package ru.mamirov.greendata.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.mamirov.greendata.model.Deposit;

public interface DepositRepository extends PagingAndSortingRepository<Deposit, Long>, JpaSpecificationExecutor<Deposit> {
}
