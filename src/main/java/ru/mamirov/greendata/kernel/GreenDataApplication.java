package ru.mamirov.greendata.kernel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
@EntityScan(basePackages = "ru.mamirov.greendata.model")
@EnableJpaRepositories(basePackages = "ru.mamirov.greendata.dao")
@ComponentScan(basePackages = "ru.mamirov.greendata")
public class GreenDataApplication {

    public static void main(String [] args) {
        SpringApplication.run(GreenDataApplication.class, args);
    }
}
