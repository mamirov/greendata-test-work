package ru.mamirov.greendata.controller;

import org.junit.Test;
import ru.mamirov.greendata.TestBase;
import ru.mamirov.greendata.model.Bank;

import java.util.List;
import java.util.Map;

public class BankControllerTest extends TestBase {

    public static final String BASE_URL = "/api/v1/bank";

    @Test
    public void testCreateBank() {
        Bank bank = getRestTemplate().postForObject(BASE_URL, getDefaultBank(), Bank.class);

        assertTrue(bank.getId() != null);
    }

    @Test
    public void testCreateAndDeleteBank() {
        Bank bank = getRestTemplate().postForObject(BASE_URL, getDefaultBank(), Bank.class);
        Long createdId = bank.getId();
        getRestTemplate().delete(BASE_URL + "/" + createdId);

        bank = getRestTemplate().getForObject(BASE_URL + "/" +createdId, Bank.class);
        assertTrue(bank == null);
    }

    @Test
    public void testUpdateBank() {
        Bank bank = getRestTemplate().postForObject(BASE_URL, getDefaultBank(), Bank.class);
        Long createdId = bank.getId();
        assertEquals("TestBank", bank.getName());

        bank.setName("Test2Bank");
        bank = getRestTemplate().postForObject(BASE_URL, bank, Bank.class);
        assertEquals("Test2Bank", bank.getName());
        assertEquals(createdId, bank.getId());
    }

    @Test
    public void testFilterBanks() {
        getRestTemplate().postForObject(BASE_URL, getDefaultBank(), Bank.class);

        Bank secondBank = getDefaultBank();
        secondBank.setName("TosterBank");
        getRestTemplate().postForObject(BASE_URL, secondBank, Bank.class);

        List<Bank> banks = getRestTemplate().getForObject(BASE_URL, List.class);
        assertTrue(banks.size() > 1);

        banks = getRestTemplate().getForObject(BASE_URL + "?name=TosterBank", List.class);
        assertEquals(1, banks.size());
        Map<String, Object> bank = (Map<String, Object>) banks.get(0);
        assertEquals("Diff name bankss", "TosterBank", bank.get("name").toString());
    }

    public Bank getDefaultBank() {
        Bank bank = new Bank();
        bank.setName("TestBank");
        bank.setBik(123345);

        return bank;
    }
}
