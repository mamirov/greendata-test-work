package ru.mamirov.greendata.controller;

import org.junit.Test;
import org.postgresql.util.PSQLException;
import ru.mamirov.greendata.TestBase;
import ru.mamirov.greendata.model.Bank;
import ru.mamirov.greendata.model.Client;
import ru.mamirov.greendata.model.Deposit;
import ru.mamirov.greendata.model.OrgLawType;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public class DepositControllerTest extends TestBase {

    public static final String BASE_URL = "/api/v1/deposit";
    public static final String BASE_URL_BANK = "/api/v1/bank";
    public static final String BASE_URL_CLIENT = "/api/v1/client";

    @Test
    public void testCreateDeposit() {
        Bank bank = getRestTemplate().postForObject(BASE_URL_BANK, getDefaultBank(), Bank.class);
        Client client = getRestTemplate().postForObject(BASE_URL_CLIENT, getDefaultClient(), Client.class);

        Deposit deposit = getRestTemplate().postForObject(BASE_URL, getDeposit(bank, client), Deposit.class);

        assertTrue(deposit.getId() != null);
    }

    @Test
    public void testWrongCreateDeposit() {
        Bank bank = getRestTemplate().postForObject(BASE_URL_BANK, getDefaultBank(), Bank.class);

        boolean isHasError = false;
        try {
            Deposit deposit = getRestTemplate().postForObject(BASE_URL, getDeposit(bank, null), Deposit.class);
        } catch (Exception e) {
            isHasError = true;
        }

        assertTrue(!isHasError);
    }

    @Test
    public void testDeleteDeposit() {
        Deposit deposit = getRestTemplate().postForObject(BASE_URL, getDefaultDeposit(), Deposit.class);
        Long createdId = deposit.getId();
        getRestTemplate().delete(BASE_URL + "/" + createdId);

        deposit = getRestTemplate().getForObject(BASE_URL + "/" +createdId, Deposit.class);
        assertTrue(deposit == null);
    }

    @Test
    public void testUpdateDeposit() {
        Deposit deposit = getRestTemplate().postForObject(BASE_URL, getDefaultDeposit(), Deposit.class);
        Long createdId = deposit.getId();
        assertEquals(new BigDecimal("11.9"), deposit.getPercent());

        deposit.setPercent(new BigDecimal("50.9"));
        deposit = getRestTemplate().postForObject(BASE_URL, deposit, Deposit.class);
        assertEquals(new BigDecimal("50.9"), deposit.getPercent());
        assertEquals(createdId, deposit.getId());
    }

    @Test
    public void testFilterDeposits() {
        getRestTemplate().postForObject(BASE_URL, getDefaultDeposit(), Deposit.class);

        Deposit secondDeposit = getDefaultDeposit();
        secondDeposit.setExpiration(36);
        getRestTemplate().postForObject(BASE_URL, secondDeposit, Deposit.class);

        List<Deposit> deposits = getRestTemplate().getForObject(BASE_URL, List.class);
        assertEquals(2, deposits.size());

        deposits = getRestTemplate().getForObject(BASE_URL + "?expiration=36", List.class);
        assertEquals(1, deposits.size());
        Map<String, Object> bank = (Map<String, Object>) deposits.get(0);
        assertEquals("Diff name bankss", 36, bank.get("expiration"));
    }

    public Deposit getDeposit(Bank bank, Client client) {
        Deposit deposit = new Deposit();
        deposit.setBank(bank);
        deposit.setClient(client);
        deposit.setExpiration(12);
        deposit.setOpenDate(LocalDateTime.now());
        deposit.setPercent(new BigDecimal("11.9"));

        return deposit;
    }

    public Deposit getDefaultDeposit() {
        Bank bank = getRestTemplate().postForObject(BASE_URL_BANK, getDefaultBank(), Bank.class);
        Client client = getRestTemplate().postForObject(BASE_URL_CLIENT, getDefaultClient(), Client.class);

        return getDeposit(bank, client);

    }

    public Bank getDefaultBank() {
        Bank bank = new Bank();
        bank.setName("TestBank");
        bank.setBik(123345);

        return bank;
    }

    public Client getDefaultClient() {
        Client client = new Client();
        client.setName("Test");
        client.setShortName("tst");
        client.setAddress("TestAddress");
        client.setOrgLawType(OrgLawType.A);

        return client;
    }
}
