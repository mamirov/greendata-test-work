package ru.mamirov.greendata.controller;

import org.junit.Test;
import ru.mamirov.greendata.TestBase;
import ru.mamirov.greendata.model.Client;
import ru.mamirov.greendata.model.OrgLawType;

import java.util.List;
import java.util.Map;

public class ClientControllerTest extends TestBase {

    public static final String BASE_URL = "/api/v1/client";

    @Test
    public void testCreateClient() {
        Client client = getRestTemplate().postForObject(BASE_URL, getDefaultClient(), Client.class);

        assertTrue(client.getId() != null);
    }

    @Test
    public void testCreateAndDeleteClient() {
        Client client = getRestTemplate().postForObject(BASE_URL, getDefaultClient(), Client.class);
        Long createdId = client.getId();
        getRestTemplate().delete(BASE_URL + "/" + createdId);

        client = getRestTemplate().getForObject(BASE_URL + "/" +createdId, Client.class);
        assertTrue(client == null);
    }

    @Test
    public void testUpdateClient() {
        Client client = getRestTemplate().postForObject(BASE_URL, getDefaultClient(), Client.class);
        Long createdId = client.getId();
        assertEquals(OrgLawType.A, client.getOrgLawType());

        client.setOrgLawType(OrgLawType.B);
        client = getRestTemplate().postForObject(BASE_URL, client, Client.class);
        assertEquals(OrgLawType.B, client.getOrgLawType());
        assertEquals(createdId, client.getId());
    }

    @Test
    public void testFilterClients() {
        getRestTemplate().postForObject(BASE_URL, getDefaultClient(), Client.class);

        Client secondClient = getDefaultClient();
        secondClient.setName("Toster");
        getRestTemplate().postForObject(BASE_URL, secondClient, Client.class);

        List<Client> clients = getRestTemplate().getForObject(BASE_URL, List.class);
        assertEquals(3, clients.size());

        clients = getRestTemplate().getForObject(BASE_URL + "?name=Toster", List.class);
        assertEquals(1, clients.size());
        Map<String, Object> client = (Map<String, Object>) clients.get(0);
        assertEquals("Diff name clients", "Toster", client.get("name").toString());
    }

    public Client getDefaultClient() {
        Client client = new Client();
        client.setName("Test");
        client.setShortName("tst");
        client.setAddress("TestAddress");
        client.setOrgLawType(OrgLawType.A);

        return client;
    }
}
