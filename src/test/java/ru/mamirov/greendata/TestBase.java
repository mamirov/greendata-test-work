package ru.mamirov.greendata;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.mamirov.greendata.kernel.GreenDataApplication;
import ru.mamirov.greendata.kernel.config.GDConfig;
import ru.mamirov.greendata.kernel.config.TestGDConfig;
import ru.mamirov.greendata.model.Client;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {GreenDataApplication.class, TestGDConfig.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestBase extends Assert {

    @Autowired
    private TestRestTemplate restTemplate;

    public TestRestTemplate getRestTemplate() {
        return restTemplate;
    }

    public void setRestTemplate(TestRestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
}
